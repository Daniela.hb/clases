class Convertidor
{
    valor;
    valor1;
    valor2;
    resultado;
    grad2rad()
    {
        this.resultado="-------Grados en radianes:\n "+this.valor*(Math.PI/180);
        return this.resultado;

    }

    cal2fahr()
    {
        this.resultado="-------Grados celsius a fahrenheit: \n"+(this.valor1*1.8)+32;
        return this.resultado;

    }

    inch2cm()
    {
        this.resultado="-------Pies en centimetros: \n"+this.valor2*2.54;
        return this.resultado;

    }
    constructor()
    {
        this.valor=0;
        this.valor1=0;
        this.valor2=0;
        this.resultado=0;
    }
}